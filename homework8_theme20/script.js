"use strict";
//----------------------------
//это уже работает в строгом режиме

function fibonacci() {
  this.prev = this.prev ? this.prev : 0;
  this.next = this.next ? this.next : 1;

  function getNextNum() {
    let newNext = this.prev + this.next;
    this.prev = this.next;
    this.next = newNext;
    console.log(newNext);
  }
  getNextNum = getNextNum.bind(this);
  getNextNum();
}

fibonacci = fibonacci.bind(fibonacci);

fibonacci();
fibonacci();
fibonacci();
fibonacci();

//----------------------------
//и это уже работает в строгом режиме

function fibonacci2() {
  this.prev = this.prev ? this.prev : 0;
  this.next = this.next ? this.next : 1;

  const getNextNum = () => {
    let newNext = this.prev + this.next;
    this.prev = this.next;
    this.next = newNext;
    console.log(newNext);
  };
  getNextNum();
}

fibonacci2 = fibonacci2.bind(fibonacci2);

fibonacci2();
fibonacci2();
fibonacci2();
fibonacci2();

//----------------------------
//решение варианта без звездочки *

// "use strict";
// function makeFibonacciFunction() {
//   let prev = 0;
//   let next = 1;

//   function getNextNum() {
//     let newNext = prev + next;
//     prev = next;
//     next = newNext;
//     console.log(newNext);
//   }
//   return getNextNum;
// }

// const fibonacci = makeFibonacciFunction();

// fibonacci();
// fibonacci();
// fibonacci();
// fibonacci();

//----------------------------
//сэнсей вариант

// "use strict";
// const fibonacci = (function () {
//   let first = 0;
//   let second = 1;

//   return () => {
//     let sum = first + second;
//     first = second;
//     second = sum;
//     console.log(sum);
//   };
// })();

// fibonacci();
// fibonacci();
// fibonacci();
// fibonacci();
