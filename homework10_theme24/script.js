"use strict";

//===== functions =====
function validateEmail(email) {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}
function getParentAndErrorTextElements(node) {
  if (node) {
    const parent = node.closest('[data-input-with-errors="true"]');
    return [parent, parent.querySelector('[data-error="true"]')];
  }
  throwError(
    "не предоставлена дом-нода, не могу найти элемент для записи ошибки"
  );
  return null;
}
function changeTextAndElementsColorSchema(
  errorText,
  parentElement,
  errorElement
) {
  errorElement.textContent = errorText;
  parentElement.dataset.haserrors = `${!!errorText}`;
}
//===== ========= =====

const registrationForm = document.querySelector(
  '[data-registration-form="true"]'
);

registrationForm.addEventListener("submit", (e) => {
  e.preventDefault();

  const errors = {
    email: "",
    password: "",
    checkboxRules: "",
  };

  const emailInput = registrationForm.querySelector("#email-input");
  const passwordInput = registrationForm.querySelector("#password-input");
  const checkRulesInput = registrationForm.querySelector("#rules-input");

  const [parentElementEmail, errorElementEmail] = [
    ...getParentAndErrorTextElements(emailInput),
  ];
  const [parentElementPassword, errorElementPassword] = [
    ...getParentAndErrorTextElements(passwordInput),
  ];
  const [parentElementCheckRules, errorElementCheckRules] = [
    ...getParentAndErrorTextElements(checkRulesInput),
  ];

  emailInput.addEventListener("input", () => {
    changeTextAndElementsColorSchema("", parentElementEmail, errorElementEmail);
  });
  passwordInput.addEventListener("input", () => {
    changeTextAndElementsColorSchema(
      "",
      parentElementPassword,
      errorElementPassword
    );
  });
  checkRulesInput.addEventListener("change", () => {
    changeTextAndElementsColorSchema(
      "",
      parentElementCheckRules,
      errorElementCheckRules
    );
  });

  switch (true) {
    case emailInput.value === "":
      errors.email = "Поле обязательно для заполнения";
      break;
    case validateEmail(emailInput.value) === false:
      errors.email = "Email не валидный";
      break;
    default:
      errors.email = "";
      break;
  }

  switch (true) {
    case passwordInput.value === "":
      errors.password = "Поле обязательно для заполнения";
      break;
    case passwordInput.value.length < 8:
      errors.password = "Пароль должен содержать как минимум 8 символов";
      break;
    default:
      errors.password = "";
      break;
  }

  if (!checkRulesInput.checked) {
    errors.checkboxRules = "Поле обязательно для заполнения";
  } else {
    errors.checkboxRules = "";
  }

  changeTextAndElementsColorSchema(
    errors.email,
    parentElementEmail,
    errorElementEmail
  );
  changeTextAndElementsColorSchema(
    errors.password,
    parentElementPassword,
    errorElementPassword
  );
  changeTextAndElementsColorSchema(
    errors.checkboxRules,
    parentElementCheckRules,
    errorElementCheckRules
  );

  const isErrors = !!Object.values(errors).find((error) => error !== "");

  if (!isErrors) {
    const user = {
      email: emailInput.value,
      password: passwordInput.value,
    };
    console.log("User = ", user);
  }
});
