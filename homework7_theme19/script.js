window.onload = function () {
  const firstInput = prompt("Введите первое число");
  const firstNumber = firstInput ? Number(firstInput.trim()) : "";

  const signInput = prompt("Введите знак (+, -, *, /)");
  const sign = signInput.trim();

  const secondInput = prompt("Введите второе число");
  const secondNumber = secondInput ? Number(secondInput.trim()) : "";

  let errors = "";

  if (isNaN(firstNumber) || isNaN(secondNumber)) {
    errors += `Некорректный ввод чисел: ${firstInput} и ${secondInput} \n`;
  } else {
    if (firstNumber === "") {
      errors += "Первое число не указано\n";
    }
    if (secondNumber === "") {
      errors += "Второе число не указано\n";
    }
  }

  if (!sign) {
    errors += "Не введён знак\n";
  } else if (!(sign === "+" || sign === "-" || sign === "*" || sign === "/")) {
    errors += `Программа не поддерживает такую математическую операцию: ${sign}\n`;
  } else if (sign === "/" && secondNumber === 0) {
    errors += `На 0 делить низзя\n`;
  }

  if (errors) {
    console.log(errors);
  } else {
    let result;
    switch (sign) {
      case "+":
        result = firstNumber + secondNumber;
        break;
      case "-":
        result = firstNumber - secondNumber;
        break;
      case "*":
        result = firstNumber * secondNumber;
        break;
      case "/":
        result = firstNumber / secondNumber;
        break;
    }
    console.log(`Результат вычисления равен ${result}`);
  }
};
