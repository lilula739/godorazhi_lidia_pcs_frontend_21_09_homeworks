"use strict";
//===== FUNCTIONS =====
function getUsersNames(usersArray, needFullNames = false) {
  return usersArray
    .reduce((acc, user) => {
      const firstName = needFullNames ? `${user.first_name} ` : "";
      return acc + firstName + user.last_name + ", ";
    }, "")
    .slice(0, -2);
}

function getFirstLetterLowerCase(str) {
  return str.slice(0, 1).toLowerCase();
}

function getUsersLastnamesStartWithLetter(usersArray, letter = "F") {
  return usersArray.filter((user) => {
    return getFirstLetterLowerCase(user.last_name) === letter.toLowerCase();
  });
}

function arrayOfObjectsToStr(arr) {
  return arr
    .map((item) => {
      return Object.entries(item)
        .map(([key, value]) => {
          return `${key}: ${value}`;
        })
        .join("; ");
    })
    .join("</br>");
}
//===== ======== =====

//===== DOM-nodes =====

const allUsersNode = document.querySelector('[data-all-users="true"]');
const usersLastnamesNode = document.querySelector(
  '[data-users-lastnames="true"]'
);
const usersFirstLetterNode = document.querySelector(
  '[data-users-first-letter="true"]'
);
const usersNamesNode = document.querySelector('[data-users-names="true"]');
const userKeysNode = document.querySelector('[data-user-keys="true"]');

//===== ========= =====

//===== API =====
const API_URL = "https://reqres.in/api/users?per_page=12";
//===== === =====

let usersData = [];

fetch(API_URL)
  .then((response) => response.json())
  .then((data) => {
    usersData = data.data;
    console.warn(
      "==============\nПолучить данные всех пользователей из https://reqres.in/api/users\n==============\n"
    );
    console.log(usersData);
    allUsersNode.insertAdjacentHTML(
      "afterbegin",
      arrayOfObjectsToStr(usersData)
    );

    console.warn(
      "==============\nВывести в консоль фамилии всех пользователей в цикле\n==============\n"
    );
    const usersLastnames = getUsersNames(usersData);
    console.log(usersLastnames);
    usersLastnamesNode.textContent = usersLastnames;

    console.warn(
      "==============\nВывести все данные всех пользователей, фамилия которых начинается на F\n==============\n"
    );
    const lastnamesStartWithLetter =
      getUsersLastnamesStartWithLetter(usersData);
    console.log(lastnamesStartWithLetter);
    usersFirstLetterNode.insertAdjacentHTML(
      "afterbegin",
      arrayOfObjectsToStr(lastnamesStartWithLetter)
    );

    console.warn(
      "==============\nВывести следующее предложение: Наша база содержит данные следующих пользователей: и далее в этой же строке через запятую имена и фамилии всех пользователей. Использовать метод reduce\n==============\n"
    );
    const usersNames = getUsersNames(usersData, true);
    console.log(
      `Наша база содержит данные следующих пользователей: ${usersNames}`
    );
    usersNamesNode.textContent = usersNames;

    console.warn(
      "==============\nВывести названия всех ключей в объекте пользователя.\n==============\n"
    );
    const userKeys = Object.keys(usersData[0]).join(", ");
    console.log(userKeys);
    userKeysNode.textContent = userKeys;
  })
  .catch((e) => {
    console.log("Что-то пошло не так", e);
  });
